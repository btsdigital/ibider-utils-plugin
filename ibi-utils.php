<?php
/**
 * Plugin Name: iBider Utils
 * Description: Utilities
 * Author: alejandro.sotoca@bts.com
 * Text Domain: ibider
 * Version: 1.2
 */

 /**
  * The core plugin class that is used to define internationalization,
  * admin-specific hooks, and public-facing site hooks.
  */

class IBI_Utils {

	private static $error_reporting = 0;
	private static $display_errors  = 0;
	public static $textdomain = 'ibider';

	public static function init() {

		// Enqueue scripts
		add_action( 'wp_enqueue_scripts', array( 'IBI_Utils', 'register_scripts' ) );

		// Shortcodes
		add_shortcode( 'ibi_user_has_role', array( 'IBI_Utils', 'shortcode_user_has_role' ) );

		add_shortcode( 'ibi_user_has_role_in', array( 'IBI_Utils', 'shortcode_user_has_role_in' ) );

		add_shortcode( 'ibi_get_user_roles', array( 'IBI_Utils', 'shortcode_get_user_roles' ) );

		add_shortcode( 'ibi_get_user_roles_in', array( 'IBI_Utils', 'shortcode_get_user_roles_in' ) );

		add_shortcode( 'ibi_user_can', array( 'IBI_Utils', 'shortcode_user_can' ) );

		add_shortcode( 'ibi_user_id', array( 'IBI_Utils', 'shortcode_user_id' ) );

		add_shortcode( 'ibi_user_email', array( 'IBI_Utils', 'shortcode_user_email' ) );

		add_shortcode( 'ibi_user_first_name', array( 'IBI_Utils', 'shortcode_user_first_name' ) );

		add_shortcode( 'ibi_user_last_name', array( 'IBI_Utils', 'shortcode_user_last_name' ) );

		add_shortcode( 'ibi_user_nicename', array( 'IBI_Utils', 'shortcode_user_nicename' ) );

		add_shortcode( 'ibi_user_login', array( 'IBI_Utils', 'shortcode_user_login' ) );

		add_shortcode( 'ibi_get_cimy_field', array( 'IBI_Utils', 'shortcode_get_cimy_field' ) );

		add_shortcode( 'ibi_cimy_field_value_match', array( 'IBI_Utils', 'shortcode_cimy_field_value_match' ) );

		add_shortcode( 'ibi_get_user_meta', array( 'IBI_Utils', 'get_user_meta_shortcode' ) );

		add_shortcode( 'ibi_logout_url', array( 'IBI_Utils', 'logout_url_shortcode' ) );

		add_shortcode( 'ibi_current_date', array( 'IBI_Utils', 'current_date_shortcode' ) );

		add_shortcode( 'ibi_timeout_test', array( 'IBI_Utils', 'timeout_test_shortcode' ) );

		// Register Ajax Actions
		add_action( 'admin_menu', array( 'IBI_Utils', 'add_bulk_user_unsubscribe_page' ) );
		add_action( 'admin_menu', array( 'IBI_Utils', 'add_bulk_user_upload_page' ) );

		add_action( 'wp_ajax_cimy_update_json', array( 'IBI_Utils', 'ajax_cimy_update_json' ) );
		add_action( 'wp_ajax_nopriv_cimy_update_json', array( 'IBI_Utils', 'ajax_cimy_update_json' ) );

		add_action( 'wp_ajax_cimy_get_json', array( 'IBI_Utils', 'ajax_cimy_get_json' ) );
		add_action( 'wp_ajax_nopriv_cimy_get_json', array( 'IBI_Utils', 'ajax_cimy_get_json' ) );

		add_action( 'wp_ajax_cimy_update_field', array( 'IBI_Utils', 'ajax_cimy_update_field' ) );
		add_action( 'wp_ajax_nopriv_cimy_update_field', array( 'IBI_Utils', 'ajax_cimy_update_field' ) );

		add_action( 'wp_ajax_cimy_get_field', array( 'IBI_Utils', 'ajax_cimy_get_field' ) );
		add_action( 'wp_ajax_nopriv_cimy_get_field', array( 'IBI_Utils', 'ajax_cimy_get_field' ) );

		add_action( 'wp_ajax_add_user_meta', array( 'IBI_Utils', 'ajax_add_user_meta' ) );
		add_action( 'wp_ajax_nopriv_add_user_meta', array( 'IBI_Utils', 'ajax_add_user_meta' ) );

		add_filter( 'the_privacy_policy_link', array( 'IBI_Utils', 'privacy_policy_link' ) );

	}


	/**
	 * Custom privacy policy link
	 *
	 * @return string
	 */
	public static function privacy_policy_link() {
		return sprintf(
			'<a class="ibi-privacy-policy-link" href="%s" target="_blank">%s</a>',
			'https://bts.com/privacy-policy/',
			__( 'BTS Data Privacy and Cookie Policy', 'ibider' )
		);
	}

	public static function enable_error_reporting() {
		// Save old status
		self::$display_errors = ini_get( 'display_errors' );
		ini_set( 'display_errors', 1 );
		// Save old level
		self::$error_reporting = error_reporting( E_ALL );

	}

	public static function disable_error_reporting() {
		error_reporting( 0 );
		ini_set( 'display_errors', 0 );
	}

	public static function restore_error_reporting() {
		error_reporting( self::$error_reporting );
		ini_set( 'display_errors', self::$display_errors );
	}

	public static function console_log( $data, $console_method = 'log' ) {
		$valid_methods = array(
			'log',
			'warn',
			'error',
			'info',
			'debug',
		);
		if ( ! in_array( $console_method, $valid_methods ) ) {
			$console_method = 'log';
		}
		printf(
			'<script>console.%s(%s)</script>',
			$console_method,
			json_encode( $data )
		);
	}

	/**
	 * Search for template files overriding the default in the active theme
	 * directory
	 *
	 * @param  String $filename Template file name
	 *
	 * @return String  Template path
	 */

	public static function get_template( $filename ) {
		try {
			$theme_directories = array(
				'ibi-templates',
				'ibi-overrides',
			);
			foreach ( $theme_directories as $directory ) {
				$file_path = sprintf(
					'%s/%s/%s',
					get_stylesheet_directory(),
					$directory,
					$filename
				);
				if ( file_exists( $file_path ) ) {
					  return $file_path;
				}
			}
			// If no template found in current_theme
			if ( file_exists( plugin_dir_path( __FILE__ ) . '../includes/' . $filename ) ) {
				return plugin_dir_path( __FILE__ ) . '../includes/' . $filename;
			}
			// If no template found in plugin neither
			throw new Exception( 'Template not found' );
		} catch ( \Exception $e ) {
			// Log?
			throw $e;
		}
	}

	private static function include_template( $filename ) {
		try {
			$path = self::get_template( $filename );
			include $path;
		} catch ( \Exception $e ) {
			throw $e;
		}
	}

	/**
	 * Ajax Cimy Fields functions
	 */

	public static function register_scripts() {
		wp_register_script(
			'ibi-utils-scripts',
			plugin_dir_url( __FILE__ ) . 'public/js/ibi-utils.js',
			array(),
			false,
			false
		);
		wp_enqueue_script( 'ibi-utils-scripts' );
		wp_localize_script(
			'ibi-utils-scripts',
			'ibi_utils_data',
			array(
				'ajax_url'        => admin_url( 'admin-ajax.php' ),
				'nonce'           => wp_create_nonce( 'ibi-utils' ),
				'current_user_id' => get_current_user_id(),
			)
		);
	}

	public static function ajax_cimy_update_json() {

		try {
			$required_fields = array(
				'field_name',
				'user_id',
				'key',
				'value',
			);
			foreach ( $required_fields as $field ) {
				if ( ! isset( $_POST[ $field ] ) ) {
					throw new \Exception( $field . ' field is undefined', 400 );
				}
			}

			$json = get_cimyFieldValue( $_POST['user_id'], $_POST['field_name'] );
			if ( empty( $json ) ) {
				$data = array();
			} else {
				$data = json_decode( $json, true );
			}
			if ( is_null( $data ) ) {
				throw new \Exception( 'Unable to parse ' . $_POST['field_name'] . ' as JSON', 400 );
			}
			$data[ $_POST['key'] ] = $_POST['value'];
			$json                  = json_encode( $data );
			set_cimyFieldValue( $_POST['user_id'], $_POST['field_name'], $json );
			$response = $data;
			wp_send_json_success( $response );
		} catch ( \Exception $e ) {
			wp_send_json_error( $e->getMessage(), $e->getCode() );
		}
	}

	public static function ajax_cimy_update_field() {

		try {
			$required_fields = array(
				'field_name',
				'user_id',
				'value',
			);
			foreach ( $required_fields as $field ) {
				if ( ! isset( $_POST[ $field ] ) ) {
					throw new \Exception( $field . ' field is undefined', 400 );
				}
			}

			// Check if user_id exists
			$user = get_user_by( 'ID', $_POST['user_id'] );

			if ( ! $user ) {
				throw new \Exception( 'User not found by id: ' . $_POST['user_id'], 400 );
			}

			// check if field_name exists
			if ( empty( $_POST['field_name'] ) || is_null( get_cimyFieldValue( $_POST['user_id'], $_POST['field_name'] ) ) ) {
				throw new \Exception( 'Invalid field name: ' . $_POST['field_name'], 400 );
			}

			set_cimyFieldValue( $_POST['user_id'], $_POST['field_name'], $_POST['value'] );
			wp_send_json_success( $_POST['value'] );
		} catch ( \Exception $e ) {
			wp_send_json_error( $e->getMessage(), $e->getCode() );
		}
	}

	public static function ajax_cimy_get_field() {

		try {
			$required_fields = array(
				'field_name',
				'user_id',
			);
			foreach ( $required_fields as $field ) {
				if ( ! isset( $_POST[ $field ] ) ) {
					throw new \Exception( $field . ' field is undefined', 400 );
				}
			}

			// Check if user_id exists
			$user = get_user_by( 'ID', $_POST['user_id'] );

			if ( ! $user ) {
				throw new \Exception( 'User not found by id: ' . $_POST['user_id'], 400 );
			}

			// check if field_name exists
			if ( empty( $_POST['field_name'] ) ) {
				throw new \Exception( 'Field name is undefined', 400 );
			}

			$value = get_cimyFieldValue( $_POST['user_id'], $_POST['field_name'] );

			wp_send_json_success( $value );
		} catch ( \Exception $e ) {
			wp_send_json_error( $e->getMessage(), $e->getCode() );
		}
	}

	public static function ajax_cimy_get_json() {

		try {
			$required_fields = array(
				'field_name',
				'user_id',
			);
			foreach ( $required_fields as $field ) {
				if ( ! isset( $_POST[ $field ] ) ) {
					throw new \Exception( $field . ' field is undefined', 400 );
				}
			}

			$json = get_cimyFieldValue( $_POST['user_id'], $_POST['field_name'] );
			if ( empty( $json ) ) {
				$response = array();
			} else {
				$response = json_decode( $json, true );
			}
			if ( is_null( $response ) ) {
				throw new \Exception( 'Unable to parse ' . $_POST['field_name'] . ' as JSON', 400 );
			}
			wp_send_json_success( $response );
		} catch ( \Exception $e ) {
			wp_send_json_error( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * Add new value in User Meta Field
	 */

	public static function ajax_add_user_meta() {
		try {
			$required_fields = array(
				'meta_key',
				'meta_value',
			);
			foreach ( $required_fields as $field ) {
				if ( ! isset( $_POST[ $field ] ) ) {
					throw new \Exception( $field . ' field is undefined', 400 );
				}
			}
			if ( ! empty( $_POST['user_id'] ) ) {
				$user = get_user_by( 'ID', $_POST['user_id'] );
			} else {
				$user = wp_get_current_user();
			}
			if ( ! $user ) {
				throw new \Exception( 'User not found', 400 );
			}
			// Get all values
			$values = get_user_meta( $user->ID, $_POST['meta_key'], false );
			// If the current value does not exist
			if ( false === array_search( $_POST['meta_value'], $values ) ) {
				add_user_meta( $user->ID, $_POST['meta_key'], $_POST['meta_value'] );
			}
			wp_send_json_success( $_POST['meta_value'] );
		} catch ( \Exception $e ) {
			wp_send_json_error( $e->getMessage(), $e->getCode() );
		}
	}

	public static function shortcode_user_has_role( $atts = array(), $content = null ) {

		$default_atts = array(
			'role'   => '',
			'output' => 'bool', // "bool" or "string"
		);

		$atts = shortcode_atts( $default_atts, $atts );

		if ( empty( $atts['role'] ) ) {
			return;
		}

		$user = wp_get_current_user();

		if ( ! $user ) {
			return $atts['output'] == 'string' ? 'false' : false;
		}

		if ( ! empty( preg_grep( '/' . $atts['role'] . '/i', $user->roles ) ) ) {
			return $atts['output'] == 'string' ? 'true' : true;
		} else {
			return $atts['output'] == 'string' ? 'false' : false;
		}
	}

	public static function shortcode_user_has_role_in( $atts = array(), $content = null ) {

		$default_atts = array(
			'roles'  => '', // Comma separated role slugs
			'output' => 'bool', // "bool" or "string"
		);

		$atts = shortcode_atts( $default_atts, $atts );

		if ( empty( $atts['roles'] ) ) {
			return;
		}

		$roles = preg_split( '/[\s,]+/', $atts['roles'] );

		$user = wp_get_current_user();

		if ( ! $user ) {
			return;
		}

		if ( ! empty( array_intersect( $roles, $user->roles ) ) ) {
			return $atts['output'] == 'string' ? 'true' : true;
		} else {
			return $atts['output'] == 'string' ? 'false' : false;
		}
	}

	public static function shortcode_get_user_roles_in( $atts = array(), $content = null ) {

		$default_atts = array(
			'roles'  => '', // Comma separated role slugs
			'output' => 'bool', // "bool" or "string"
		);

		$atts = shortcode_atts( $default_atts, $atts );

		if ( empty( $atts['roles'] ) ) {
			return;
		}

		$roles = preg_split( '/[\s,]+/', $atts['roles'] );

		$user = wp_get_current_user();

		if ( ! $user ) {
			return;
		}

		$matching_roles = array_intersect( $roles, $user->roles );

		return ( $atts['output'] == 'string' ) ? implode( ', ', $matching_roles ) : $matching_roles;
	}

	public static function shortcode_get_user_roles( $atts = array(), $content = null ) {

		$default_atts = array(
			'output' => 'bool', // "bool" or "string"
		);

		$atts = shortcode_atts( $default_atts, $atts );

		$user = wp_get_current_user();

		if ( ! $user ) {
			return;
		}

		return ( $atts['output'] == 'string' ) ? implode( ', ', $user->roles ) : $user->roles;
	}

	/**
	 * Checks whether a user has the given capability or not.
	 *
	 * @return Bool/String depending on the "output" parameter
	 */

	public static function shortcode_user_can( $atts = array(), $content = null ) {

		$default_atts = array(
			'capability' => '',
			'user_id'    => get_current_user_id(),
			'output'     => 'bool', // "bool" or "string"
		);

		$atts = shortcode_atts( $default_atts, $atts );

		if ( empty( $atts['capability'] ) ) {
			return;
		}

		$user = get_user_by( 'ID', $atts['user_id'] );

		if ( ! $user ) {
			return ( $atts['output'] == 'string' ) ? 'false' : false;
		}

		if ( $user->has_cap( $atts['capability'] ) ) {
			return ( $atts['output'] == 'string' ) ? 'true' : true;
		} else {
			return ( $atts['output'] == 'string' ) ? 'false' : false;
		}

	}


	public static function shortcode_user_id() {

		return get_current_user_id();

	}

	public static function shortcode_user_email( $atts = array(), $content = null ) {

		$default_atts = array(
			'user_id' => get_current_user_id(),
		);

		$atts = shortcode_atts( $default_atts, $atts );

		$user = get_user_by( 'ID', $atts['user_id'] );

		if ( ! $user ) {
			return;
		}

		return $user->user_email;

	}

	public static function shortcode_user_first_name( $atts = array(), $content = null ) {

		$default_atts = array(
			'user_id' => get_current_user_id(),
		);

		$atts = shortcode_atts( $default_atts, $atts );

		return get_user_meta( $atts['user_id'], 'first_name', true );

	}

	public static function shortcode_user_last_name( $atts = array(), $content = null ) {

		$default_atts = array(
			'user_id' => get_current_user_id(),
		);

		$atts = shortcode_atts( $default_atts, $atts );

		return get_user_meta( $atts['user_id'], 'last_name', true );

	}

	/**
	 * Shortcode: Print user_login
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	public static function shortcode_user_login( $atts = array(), $content = null ) {

		$default_atts = array(
			'user_id' => get_current_user_id(),
		);

		$atts = shortcode_atts( $default_atts, $atts );

		$user = get_user_by( 'ID', $atts['user_id'] );

		if ( ! $user ) {
			return;
		}

		return $user->user_login;

	}

	/**
	 * Render [ibi_user_nicename] shortcode
	 */
	public static function shortcode_user_nicename( $atts = array(), $content = null ) {

		$default_atts = array(
			'user_id' => get_current_user_id(),
		);

		$atts = shortcode_atts( $default_atts, $atts );

		$user = get_user_by( 'ID', $atts['user_id'] );

		if ( ! $user ) {
			return;
		}

		return $user->user_nicename;

	}

	public static function shortcode_get_cimy_field( $atts = array(), $content = null ) {

		$default_atts = array(
			'user_id' => get_current_user_id(),
			'field'   => '',
			'format'  => null,
		);

		$atts = shortcode_atts( $default_atts, $atts );

		if ( empty( $atts['field'] ) ) {
			return;
		}

		$user = get_user_by( 'ID', $atts['user_id'] );

		if ( ! $user ) {
			return;
		}

		$value = get_cimyFieldValue( $atts['user_id'], $atts['field'] );

		if ( $atts['format'] == 'string' ) {
			return empty( $value ) ? '' : (string) $value;
		} else {
			return $value;
		}

	}

	/**
	 * Shortcode
	 *
	 * @return String
	 */

	public static function shortcode_cimy_field_value_match( $atts = array(), $content = null ) {
		if ( wp_doing_ajax() || is_admin() ) {
			return;
		}
		$default_atts = array(
			'pattern' => '',
			'field'   => '',
			'user_id' => get_current_user_id(),
		);
		$atts         = shortcode_atts( $default_atts, $atts );

		if ( empty( $atts['pattern'] ) ) {
			return;
		}

		if ( empty( $atts['field'] ) ) {
			return;
		}

		$user = get_user_by( 'ID', $atts['user_id'] );

		if ( ! $user ) {
			return;
		}

		$value = get_cimyFieldValue( $atts['user_id'], $atts['field'] );

		return preg_match( $atts['pattern'], $value ) ? 'true' : 'false';
	}

	/**
	 *  Get User Meta Shortcode
	 *
	 * @return String
	 */

	public static function get_user_meta_shortcode( $atts = array(), $content = null ) {
		if ( wp_doing_ajax() || is_admin() ) {
			return;
		}
		$default_atts = array(
			'user_id'  => get_current_user_id(),
			'meta_key' => '',
			'single'   => 'true',
		);
		$atts         = shortcode_atts( $default_atts, $atts );
		try {
			if ( ! $atts['user_id'] ) {
				throw new \Exception( 'User ID is undefined' );
			}

			$meta_value = get_user_meta( $atts['user_id'], $atts['meta_key'], preg_match( '/true/i', $atts['single'] ) );

			if ( is_array( $meta_value ) ) {
				return json_encode( $meta_value );
			} else {
				return $meta_value;
			}
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}

	/**
	 * Print logout URL with nonce parameter
	 *
	 * @return String
	 */

	public static function logout_url_shortcode( $atts = array(), $content = null ) {
		if ( wp_doing_ajax() || is_admin() ) {
			return;
		}
		$default_atts = array();
		$atts         = shortcode_atts( $default_atts, $atts );
		return wp_nonce_url( site_url( 'wp-login.php?action=logout' ), 'log-out' );
	}

	/**
	 * Shortcode
	 * Output current date
	 */
	public static function current_date_shortcode( $atts = array(), $content = null ) {
		try {
			if ( wp_doing_ajax() || is_admin() ) {
				return;
			}
			$default_atts = array(
				'format' => 'l, j/n/Y H:i a e',
				'modify' => null,
			);
			$atts         = shortcode_atts( $default_atts, $atts );
			$current_date = new DateTime();
			if ( ! empty( $atts['modify'] ) ) {
				$current_date = $current_date->modify( $atts['modify'] );
			}
			return $current_date->format( $atts['format'] );
		} catch ( \Exception $e ) {
			return $e->getMessage();
		}
	}


	/**
	 * Shortcode: Execution time tester
	 *
	 * @param array  $atts
	 * @param string $content
	 *
	 * @return string
	 */
	public static function timeout_test_shortcode( $atts = array(), $content = null ) {

		try {

			if ( wp_doing_ajax() || is_admin() ) {
				return;
			}
			$default_atts = array();
			$atts         = shortcode_atts( $default_atts, $atts );

			$max_time = isset( $_POST['ibi_timeout']['max_time'] ) ? $_POST['ibi_timeout']['max_time'] : ini_get( 'max_execution_time' );
			$time     = isset( $_POST['ibi_timeout']['time'] ) ? $_POST['ibi_timeout']['time'] : 2;

			ob_start(); ?>

			<form action="" method="post">
				<label for="ibiMaxExecutionTime">PHP Max Execution Time</label><br>
				<input type="number" name="ibi_timeout[max_time]" id="ibiMaxExecutionTime" value="<?php echo $max_time; ?>" required><br>
				<label for="ibiTimeoutSeconds">Execution time</label><br>
				<input type="number" name="ibi_timeout[time]" id="ibiTimeoutSeconds" value="<?php echo $time; ?>" required><br>
				<input style="margin-top: 1rem" type="submit" value="Start test">
			</form>

			<?php
			if ( isset( $_POST['ibi_timeout']['max_time'] ) ) {
				ini_set( 'max_execution_time', intval( $_POST['ibi_timeout']['max_time'] ) );
			}

			if ( isset( $_POST['ibi_timeout']['time'] ) ) {
				$start_time = time();

				sleep( intval( $_POST['ibi_timeout']['time'] ) );

				echo '<br><strong>Test result:</strong><br>';
				$max_execution_time = ini_get( 'max_execution_time' );
				echo "Max Execution Time: {$max_execution_time}s<br>";
				echo "Execution Time: {$time}s<br>";
				echo 'Elapsed time: ' . ( $elapsed_time = ( time() - $start_time ) ) . 's<br>';
				echo 'Start time: ' . ( $formatted_start_time = date_create_from_format( 'U', $start_time )->format( 'r' ) ) . '<br>';

				$test_result = array(
					'max_execution_time' => $max_execution_time,
					'execution_time'     => $time,
					'elapsed_time'       => $elapsed_time,
					'start_time'         => $formatted_start_time,
				);

				set_transient( 'ibi_timeout_test_result', $test_result, DAY_IN_SECONDS );

			} else {
				$test_result = get_transient( 'ibi_timeout_test_result' );
				if ( $test_result !== false ) {
					echo '<br><strong>Last test result:</strong><br>';
					echo "Max Execution Time: {$test_result['max_execution_time']}s<br>";
					echo "Execution Time: {$test_result['execution_time']}s<br>";
					echo "Elapsed time: {$test_result['elapsed_time']}s<br>";
					echo "Start time: {$test_result['start_time']}<br>";
				} else {
					echo '<br><strong>Last test result:</strong> Empty.<br>';
				}
			}

			return ob_get_clean();

		} catch ( \Exception $e ) {
			return $e->getMessage();
		}

	}

	public static function add_bulk_user_unsubscribe_page() {
    add_submenu_page(
      'users.php',
      __( 'Bulk user unsubscribe', 'ibi-utils' ),
      'Bulk unsubscribe',
      'manage_options',
      'ibi-bulk-user-unsubscribe',
      array( 'IBI_Utils', 'render_bulk_user_unsubscribe_page'),
      null
    );
  }

	public static function render_bulk_user_unsubscribe_page() {
    require_once plugin_dir_path( __FILE__ ) . 'admin/bulk-user-unsubscribe.php';
  }

	public static function add_bulk_user_upload_page() {
		add_submenu_page(
			'users.php',
			__( 'Upload users', 'ibi-utils' ),
			'Upload users',
			'manage_options',
			'ibi-bulk-user-upload',
			array( 'IBI_Utils', 'render_bulk_user_upload_page'),
			null
		);
	}
	
	public static function render_bulk_user_upload_page() {
		require_once plugin_dir_path( __FILE__ ) . 'admin/bulk-user-upload.php';
	}

}

/**
 * Initialize
 */

IBI_Utils::init();
