<style>
.ibi-admin-table {
  border-collapse: collapse;
  border: solid 1px #e0e0e0;
}
.ibi-admin-table__col {
  border: solid 1px #e0e0e0;
  padding: 4px;
}
</style>
<div class="wrap">
  <h1><?php echo get_admin_page_title(); ?></h1>
  <p><?php _e( 'Upload a CSV file containg the emails of the users to unsubscribe.', 'ibi-utils' ); ?></p>
  <p><?php _e('Example: ', 'ibi-utils' ) ?></p>
  <table class="ibi-admin-table" cellspacing="0">
    <tr>
      <td class="ibi-admin-table__col">user@site.com</td>
    </tr>
    <tr>
      <td class="ibi-admin-table__col">user2@site.com</td>
    </tr>
    <tr>
      <td class="ibi-admin-table__col">user3@site.com</td>
    </tr>
  </table>
  <hr>
  <h2>Upload CSV file</h2>
  <form
    class="ibi-admin-form"
    action=""
    method="post"
    enctype="multipart/form-data"
    >
    <input
      id="fileToUpload"
      type="file"
      name="unsubscribe_csv_file"
      >
    <input
      class="button-primary"
      type="submit"
      name="submit"
      value="Upload"
      >
  </form>
  <?php
  if ( isset( $_POST['submit'] ) && !empty( $_FILES['unsubscribe_csv_file'] ) ) {
    try {
      $file = fopen( $_FILES['unsubscribe_csv_file']['tmp_name'], "r" );
      $file_type = strtolower( pathinfo( $_FILES['unsubscribe_csv_file']['name'], PATHINFO_EXTENSION ) );
      if ( $file_type != 'csv' ) {
        throw new \Exception("Please select a CSV file.", 1);
      }
      $errors = array();
      $warnings = array();
      $counter = 0;
      if ( !$file ) {
        throw new \Exception("Unable to open file", 1);
      }
      while ( ( $row = fgetcsv( $file ) ) !== false ) {

        if ( count( $row ) != 1 ) {
          $errors[] = 'Invalid format. The file must contain only one value per row';
          break;
        }

        $user = get_user_by( 'email', $row[0] );
        if ( !$user ){
          $errors[] = 'User not found by email: ' . $row[0];
          continue;
        }

				/**
				 * Add role "unsubscribed" to the user. Any previous roles are kept.
				 * In case the user already has the "unsubscribed" role, the add_role() method does nothing.
				 */
				if ( $user->has_cap( 'unsubscribed' ) ) {
					$warnings[] = sprintf(
						__( '%s is already unsubscribed', 'ibi-utils' ),
						$user->user_email,
					);
					continue;
				}

				// Create unsubscribe role if it does not already exist
				$wp_roles = wp_roles();
				if ( ! $wp_roles->is_role( 'unsubscribed' ) ) {
					$wp_roles->add_role(
						'unsubscribed',
						'Unsubscribed'
					);
					printf(
						'<div class="notice notice-success"><p>%s</p></div>',
						__( 'New "Unsubscribed" role registered.', 'ibi-utils' )
					);
				}

				$user->add_role( 'unsubscribed' );
				$counter++;
      }
			echo '<hr>';
			echo '<p>';
      printf(
        __( '%s users unsubscribed successfully.' , 'ibi-utils' ),
        $counter
      );
			echo '</p>';
      if ( !empty( $errors ) ) {
        echo '<p>Errors: </p>';
        echo '<ul>';
        foreach ( $errors as $error ) {
          echo '<li>' . $error . '</li>';
        }
        echo '</ul>';
      }
			if ( !empty( $warnings ) ) {
        echo '<p>Warnings: </p>';
        echo '<ul>';
        foreach ( $warnings as $warning ) {
          echo '<li>' . $warning . '</li>';
        }
        echo '</ul>';
      }
    } catch (\Exception $e) {
			printf(
				'<div class="notice notice-error"><p>%s: %s</p></div>',
				__( 'Bulk user unsubscribe failed', 'ibi-utils' ),
				$e->getMessage(),
			);
    }
  }
  ?>
</div>
