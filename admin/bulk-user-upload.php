<style>
.ibi-admin-table {
  border-collapse: collapse;
  border: solid 1px #e0e0e0;
}
.ibi-admin-table th,
.ibi-admin-table td  {
  border: solid 1px #e0e0e0;
  padding: 4px;
}
.ibi-admin-table .is-required {
	background: lightgray;
}
</style>
<div class="wrap">
  <h1><?php echo get_admin_page_title(); ?></h1>
  <p><?php _e( 'Upload a csv file containing the user data to be created. "user_email" and "user_login" are required fields.' ); ?></p>
	<p>
		<h4>Accepted columns</h4>
		<table class="ibi-admin-table" cellspacing="0">
			<tr><th>field</th><th>type</th><th>description</th></tr>
			<tr><td>ID</td><td>integer</td><td>If supplied, user will be updated.</td></tr>
			<tr><td>user_pass<td>string</td><td>Plain text user password.</td></tr>
			<tr class="is-required"><td><strong>user_login</strong><td>string</td><td><strong>(Required)</strong> The user's login username.</td></tr>
			<tr><td>user_nicename<td>string</td><td>The URL-friendly user name.</td></tr>
			<tr><td>user_url<td>string</td><td>The user URL.</td></tr>
			<tr class="is-required"><td><strong>user_email</strong><td>string</td><td><strong>(Required)</strong> The user email address.</td></tr>
			<tr><td>display_name<td>string</td><td>The user's display name. Default is the user's username.</td></tr>
			<tr><td>nickname<td>string</td><td>The user's nickname. Default is the user's username.</td></tr>
			<tr><td>first_name<td>string</td><td>The user's first name.</td></tr>
			<tr><td>last_name<td>string</td><td>The user's last name.</td></tr>
			<tr><td>description<td>string</td><td>The user's biographical description.</td></tr>
			<tr><td>rich_editing<td>string</td><td>Whether to enable the rich-editor for the user. Accepts 'true' or 'false'. Default 'true'. </td></tr>
			<tr><td>syntax_highlighting<td>string</td><td>Whether to enable the rich code editor for the user. Accepts 'true' or 'false'. Default 'true'. </td></tr>
			<tr><td>comment_shortcuts<td>string</td><td>Whether to enable comment moderation keyboard shortcuts for the user. Accepts 'true' or 'false'. Default 'true'. </td></tr>
			<tr><td>admin_color<td>string</td><td>Admin color scheme for the user. Default 'fresh'.</td></tr>
			<tr><td>use_ssl<td>bool</td><td>Whether the user should always access the admin over https. Default false.</td></tr>
			<tr><td>user_registered<td>string</td><td>Date the user registered in UTC. Format is 'Y-m-d H:i:s'.</td></tr>
			<tr><td>user_activation_key<td>string</td><td>Password reset key. Default empty.</td></tr>
			<tr><td>spam<td>bool</td><td>Multisite only. Whether the user is marked as spam.
Default false.</td></tr>
			<tr><td>show_admin_bar_front<td>string</td><td>Whether to display the Admin Bar for the user on the site's front end. Accepts 'true' or 'false' as a string literal, not boolean. Default 'true'.</td></tr>
			<tr><td>role<td>string</td><td>User's role.</td></tr>
			<tr><td>locale<td>string</td><td>User's locale. Default empty.</td></tr>
			<!-- <tr><td>meta_input<td>string</td><td>JSON formatted array of custom user meta values keyed by meta key. -->
			<tr><td>meta:&lt;meta_key&gt;<td>string</td><td>Meta value. It is possible to add multiple meta_key columns, even with the same meta key.</td></tr>
		</table>
	</p>
	<p>
		Download <a href="<?php echo plugin_dir_url( __FILE__ ) . 'bulk-user-upload-template.csv'; ?>" download>CSV template</a>
	</p>
	
  <hr>
  <h2>Upload CSV file</h2>
  <form
	class="ibi-admin-form"
	action=""
	method="post"
	enctype="multipart/form-data"
	>
	<input
	  id="fileToUpload"
	  type="file"
	  name="upload_csv_file"
	  >
	<input
	  class="button-primary"
	  type="submit"
	  name="submit"
	  value="Upload"
	  >
  </form>
  <?php
	/**
	 * Use wp_insert_user() function to create users in the csv file
	 *
	 * Columns:
	 * - 'ID' int If supplied, user will be updated.
	 * - 'user_pass' string Plain text user password.
	 * - 'user_login' string 
	 * - 'user_nicename' string
	 * - 'user_url' string
	 * - 'user_email' string
	 * - 'display_name' string Default is user's username.
	 * - 'nickname' string
	 * - 'first_name' string
	 * - 'last_name' string
	 * - 'description' string
	 * - 'rich_editing' string Accepts 'true' or 'false'. Default 'true'. 
	 * - 'syntax_highlighting' string Accepts 'true' or 'false'. Default 'true'. 
	 * - 'comment_shortcuts' string Accepts 'true' or 'false'. Default 'true'. 
	 * - 'admin_color' string Default 'fresh'.
	 * - 'use_ssl' bool Default false.
	 * - 'user_registered' string Date the user registered in UTC. Format is 'Y-m-d H:i:s'.
	 * - 'user_activation_key' string Default empty.
	 * - 'spam' bool Default false.
	 * - 'show_admin_bar_front' string Defauld: "true"
	 * - 'role' string
	 * - 'locale' string
	 * - 'meta_input' array
	 *
	 * @see https://developer.wordpress.org/reference/functions/wp_insert_user/
	 */

	if ( isset( $_POST['submit'] ) && ! empty( $_FILES['upload_csv_file'] ) ) {

		try {

			$file      = fopen( $_FILES['upload_csv_file']['tmp_name'], 'r' );
			$file_type = strtolower( pathinfo( $_FILES['upload_csv_file']['name'], PATHINFO_EXTENSION ) );
			if ( $file_type != 'csv' ) {
				throw new \Exception( 'Please select a CSV file.', 1 );
			}
			$successes = array();
			$errors   = array();
			$warnings = array();
			if ( ! $file ) {
				throw new \Exception( 'Unable to open file', 1 );
			}

			/*
      Ignore the first 3 characters in UTF8 CSV files prepended with BOM (Byte 
      order mark). This caused the first email in the file to be considered 
      invalid. The BOM characters are always the same.

      Progress file pointer and get first 3 characters to compare to the BOM string. If the BOM characters are found, the pointer will stay passed them.
      */
      if ( fgets( $file, 4 ) !== "\xef\xbb\xbf" ) {
        // BOM not found - rewind pointer to start of file.
        rewind( $file );
      }

			// Analyse header row and determine the correspondence between column number and fields.
			$headers = fgetcsv( $file );

			if ( false === $headers ) {
				throw new \Exception( __( 'The uploaded file is empty', self::$textdomain ), 1 );
			}
			$required_headers = array(
				'user_email',
				'user_login',
			);
			$valid_headers = array(
				'ID',
				'user_pass',
				'user_login',
				'user_nicename',
				'user_url',
				'user_email',
				'display_name',
				'nickname',
				'first_name',
				'last_name',
				'description',
				'rich_editing',
				'syntax_highlighting',
				'comment_shortcuts',
				'admin_color',
				'use_ssl',
				'user_registered',
				'user_activation_key',
				'spam',
				'show_admin_bar_front',
				'role',
				'locale',
				'meta_input',
			);

			foreach ( $required_headers as $required_header ) {
				if ( ! in_array( $required_header, $headers ) ) {
					throw new \Exception( 
						sprintf(
							__( 'The uploaded file is missing a required header: %s', self::$textdomain ),
							$required_header
						)
					);
				}
			}
			
			foreach ( $headers as $header ) {
				if ( ! in_array( $header, $valid_headers ) && ! preg_match( '/^meta:.+$/i', $header ) ) {
					throw new \Exception( 
						sprintf( 
							__( "Invalid header found: %s", self::$textdomain ), 
							! empty( $header ) ? sanitize_text_field( $header ) : __( '(empty)', self::$textdomain )
						) 
					);
					break;
				}
			}

			while ( ( $row = fgetcsv( $file ) ) !== false ) {

				if ( count( $row ) != count( $headers ) ) {
					$errors[] = __( 'The number columns does not match the number of headers', self::$textdomain );
					continue;
				}
				
				$args = array();

				foreach ( $row as $index => $value ) {

					$header = $headers[ $index ];
					
					// Validate email
					if ( $header === 'user_email' ) {
						if ( ! filter_var( $value, FILTER_VALIDATE_EMAIL ) ) {
							$errors[] = sprintf(
								__( "Invalid email address: %s", self::$textdomain ),
								sanitize_text_field( $value ),
							);
							continue 2; // Skip this row
						}
					}

					// Prepare meta input
					if ( $header === 'meta_input' ) {
						$value = json_decode( $value, true );
					}

					// Prepare meta columns
					$matches = array();
					if ( preg_match( '/meta:(?P<meta_key>.+)/i', $header, $matches ) ) {
						$header = 'meta_input';
						$value = array( $matches['meta_key'] => $value );
					}

					if ( ! isset( $args[ $header ] ) ) {
						$args[ $header ] = $value;
					} else if ( is_array( $args[ $header ] ) ) {
						/**
						 * array_merge_recursive merges matching key's values into an array instead of overwriting the value. 
						 * This allows setting multiple meta:<key> columns that will be later merged together.
						 */
						$args[ $header ] = array_merge_recursive( $args[ $header ] ); 
					}

				}

				// echo json_encode($args) . "<br>";
				if ( ! is_wp_error( $insert_user_result = wp_insert_user( $args ) ) ) {
					$successes[] = sprintf(
						__( '%s (%s)', self::$textdomain ),
						$args['user_login'],
						$args['user_email'],
					);
				} else {
					$errors[] = 'Failed to create user ' . $args['user_login'] . ": " . $insert_user_result->get_error_message();
				}

			}

			// Summary
			if ( ! empty( $successes ) ) {
				printf(
					'<div class="notice notice-success"><p>%s</p><p>%s</p></div>',
					sprintf ( __( "%s users created/updated", self::$textdomain ), count( $successes ) ),
					implode( "<br>", $successes ),
				);
			}
			if ( ! empty( $errors ) ) {
				printf(
					'<div class="notice notice-error"><p>%s</p><p>%s</p></div>',
					sprintf ( __( "%s operations failed", self::$textdomain ), count( $errors ) ),
					implode( "<br>", $errors ),
					
				);
			}
			if ( ! empty( $warnings ) ) {
				printf(
					'<div class="notice notice-warning"><p>%s</p><p>%s</p></div>',
					sprintf ( __( "%s warnings", self::$textdomain ), count( $warnings ) ),
					implode( "<br>", $warnings ),
					
				);
			}
		} catch ( \Exception $e ) {
			printf(
				'<div class="notice notice-error"><p>%s: %s</p></div>',
				__( 'Bulk user upload failed', 'ibi-utils' ),
				$e->getMessage(),
			);
		}
	}
	?>
</div>
