var CimyUtils = {
  getField: function ( field_name, user_id, cb ) {
    try {
      if ( typeof jQuery == 'undefined' ) {
        throw 'jQuery is undefined';
      }
      if ( typeof ibi_utils_data != 'object' ) {
        throw 'ibi_utils_data is undefined';
      }
      if ( typeof field_name == 'undefined' ) {
        throw 'field_name is required';
      }
      user_id = user_id || ibi_utils_data.current_user_id;
      if ( typeof user_id == 'undefined' || user_id == '' ) {
        throw 'user_id is undefined';
      }
      jQuery.post(
        ibi_utils_data.ajax_url,
        {
          action: "cimy_get_field",
          _ajax_nonce: ibi_utils_data.nonce,
          user_id: user_id,
          field_name: field_name
        },
        function ( data, status, xhr ) {
          if (typeof cb == 'function') {
            cb(data);
          }
        },
        'json'
      );
    } catch (e) {
      console.error(e);
    }
  },
  updateField: function ( field_name, value, user_id, cb ) {
    try {
      if ( typeof jQuery == 'undefined' ) {
        throw 'jQuery is undefined';
      }
      if ( typeof field_name == 'undefined' ) {
        throw 'field_name is required';
      }
      if ( typeof value == 'undefined' ) {
        throw 'value is required';
      }
      if ( typeof ibi_utils_data != 'object' ) {
        throw 'ibi_utils_data is undefined';
      }
      user_id = user_id || ibi_utils_data.current_user_id;
      if ( typeof user_id == 'undefined' || user_id == '' ) {
        throw 'user_id is undefined';
      }
      jQuery.post(
        ibi_utils_data.ajax_url,
        {
          action: "cimy_update_field",
          _ajax_nonce: ibi_utils_data.nonce,
          user_id: user_id,
          field_name: field_name,
          value: value
        },
        function ( data, status, xhr ) {
          if (typeof cb == 'function') {
            cb(data);
          }
        },
        'json'
      );
    } catch (e) {
      console.error(e);
    }
  },
  updateJson: function ( field_name, key, value, user_id, cb ) {
    try {
      if ( typeof jQuery == 'undefined' ) {
        throw 'jQuery is undefined';
      }
      if ( typeof field_name == 'undefined' ) {
        throw 'field_name is required';
      }
      if ( typeof key == 'undefined' ) {
        throw 'key is required';
      }
      if ( typeof value == 'undefined' ) {
        throw 'value is required';
      }
      if ( typeof ibi_utils_data != 'object' ) {
        throw 'ibi_utils_data is undefined';
      }
      user_id = user_id || ibi_utils_data.current_user_id;
      jQuery.post(
        ibi_utils_data.ajax_url,
        {
          action: "cimy_update_json",
          _ajax_nonce: ibi_utils_data.nonce,
          user_id: user_id,
          field_name: field_name,
          key: key,
          value: value
        },
        function ( data, status, xhr ) {
          if (typeof cb == 'function') {
            cb(data);
          }
        },
        'json'
      );
    } catch (e) {
      console.error(e);
    }
  },
  getJson: function ( field_name, user_id, cb ) {
    try {
      if ( typeof jQuery == 'undefined' ) {
        throw 'jQuery is undefined';
      }
      if ( typeof field_name == 'undefined' ) {
        throw 'field_name is required';
      }
      if ( typeof ibi_utils_data != 'object' ) {
        throw 'ibi_utils_data is undefined';
      }
      user_id = user_id || ibi_utils_data.current_user_id;
      jQuery.post(
        ibi_utils_data.ajax_url,
        {
          action: "cimy_get_json",
          _ajax_nonce: ibi_utils_data.nonce,
          user_id: user_id,
          field_name: field_name
        },
        function ( data, status, xhr ) {
          if (typeof cb == 'function') {
            cb(data);
          }
        },
        'json'
      );
    } catch (e) {
      console.error(e);
    }
  }
};

var IbiUtils = {
  equalHeightSelectors: [],
  equalHeightBlocked: false,
  initEqualHeight: function(selectors) {
		
		// Register equal height selectors
		switch ( typeof selectors ) {
			case 'string':
				this.equalHeightSelectors.push(selectors);
				break;
			case 'object': 
				this.equalHeightSelectors = selectors;
				break;
			case 'undefined':
				break;
			default:
				console.warn('Invalid selectors parameter');
		}
		
    document.addEventListener('DOMContentLoaded', IbiUtils.equalHeight);
    window.addEventListener('resize', IbiUtils.equalHeight);
  },
  equalHeight: function () {
    // TODO: Maximum and minimum viewport sizes
    if ( !IbiUtils.equalHeightBlocked ) {
      IbiUtils.equalHeightBlocked = true;
      
			// Get all elements with class ibi-equal-height-*
			var elements = document.querySelectorAll('[class*="ibi-equal-height-"]');
      var done = [];
      var regex = /^ibi-equal-height-(.*)/;
			
			// Remove inline height styling
     	IbiUtils.resetHeight( elements );
			
      for ( i = 0; i < elements.length; i++ ) {
        // Get the class starting with "ibi-equal-height-"
				for ( var j = 0; j < elements[i].classList.length; j++ ) {
          var matches = elements[i].classList[j].match(regex);
          if (
            matches &&
            matches.length >= 2 &&
            done.indexOf(matches[1]) == -1
          ) {
            var equalElements = document.getElementsByClassName(elements[i].classList[j]);
            if (equalElements && equalElements.length > 1) {
              var maxHeight = 0;
              for ( var k = 0; k < equalElements.length; k++) {
                if (equalElements[k].offsetHeight > maxHeight) {
                  maxHeight = equalElements[k].offsetHeight;
                }
              }
							if ( maxHeight != 0 ) {
								for (k = 0; k < equalElements.length; k++) {
									equalElements[k].style.height = maxHeight + 'px';
								}
							}
            }
            done.push(matches[1]);
          }
        }
      }
			
			// Process elements matching custom equalHeightSelectors
			for ( var selector of IbiUtils.equalHeightSelectors ) {
				// Get elements
				var elements = document.querySelectorAll( selector );
				
				// Reset height
				IbiUtils.resetHeight( elements );
				if (elements && elements.length > 1) {
					var maxHeight = 0;
					for ( var element of elements) {
						if ( element.offsetHeight > maxHeight) {
							maxHeight = element.offsetHeight;
						}
					}
					for ( var element of elements) {
						element.style.height = maxHeight + 'px';
					}
				}
			}
			
      setTimeout(function(){
        IbiUtils.equalHeightBlocked = false;
      },100);
    }
  },
	
	resetHeight: function( elements ) {
		if ( typeof elements == 'object' ) {
			// Remove inline height styling
			for ( var element of elements ) {
				element.style.height = null;
			}
		}
	},
	
  addUserMeta: function ( meta_key, meta_value, user_id, cb ) {
    try {
      if ( typeof jQuery == 'undefined' ) {
        throw 'jQuery is undefined';
      }
      if ( typeof meta_key == 'undefined' ) {
        throw 'meta_key is required';
      }
      if ( typeof meta_value == 'undefined' ) {
        throw 'meta_value is required';
      }
      if ( typeof ibi_utils_data != 'object' ) {
        throw 'ibi_utils_data is undefined';
      }
      user_id = user_id || ibi_utils_data.current_user_id;
      jQuery.post(
        ibi_utils_data.ajax_url,
        {
          action: "add_user_meta",
          _ajax_nonce: ibi_utils_data.nonce,
          user_id: user_id,
          meta_key: meta_key,
          meta_value: meta_value,
        },
        function ( data, status, xhr ) {
          if (typeof cb == 'function') {
            cb(data);
          }
        },
        'json'
      );
    } catch (e) {
      console.error(e);
    }
  }
};
